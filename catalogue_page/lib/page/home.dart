import 'package:catalogue_page/page/ui_list.dart';
import 'package:catalogue_page/widgets/searchbar.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          elevation: 0.1,
          backgroundColor: Colors.deepPurple[400],
          actions: <Widget>[
            new IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                onPressed: () {
                  showSearch(context: context, delegate: SearchBar());
                })
          ],
        ),
        body: ListView(
          children: [
            Container(
              width: double.infinity,
              height: 30,
              decoration: BoxDecoration(color: Colors.grey[200]),
            ),
            ProductPreview(
              name: "Laptop",
              price: 7000000,
            ),
            ProductPreview(
              name: "Komputer",
              price: 10000000,
            ),
            ProductPreview(
              name: "Smartphone",
              price: 4000000,
            ),
            ProductPreview(
              name: "Tablet",
              price: 5000000,
            ),
          ],
        ));
  }
}
