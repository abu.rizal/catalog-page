import 'package:catalogue_page/page/ui_detail.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ProductPreview extends StatelessWidget {
  final String name;
  final double price;

  ProductPreview({this.name, this.price});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new uiDetail())),
        child: Container(
          padding: EdgeInsets.all(10),
          height: 160,
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(color: Colors.grey[200], width: 2))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 3,
                child: Container(
                  width: 120,
                  height: 120,
                  decoration: BoxDecoration(color: Colors.deepPurple[400]),
                  child: FlutterLogo(),
                ),
              ),
              Expanded(
                  flex: 7,
                  child: Container(
                      padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                      height: 120,
                      child: Stack(children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            new Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                                child: Text(
                                  name,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                )),
                            new Container(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                              child: Text(NumberFormat.currency(
                                      locale: 'id', decimalDigits: 0)
                                  .format(price)),
                            ),
                            new Row(
                              children: [
                                Icon(
                                  Icons.location_on,
                                  color: Colors.grey[600],
                                  size: 20,
                                ),
                                Text(
                                  "location",
                                  style: TextStyle(color: Colors.grey[600]),
                                )
                              ],
                            )
                          ],
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: Container(
                            width: 50,
                            height: 30,
                            decoration:
                                BoxDecoration(color: Colors.deepPurple[400]),
                            child: Center(
                              child: Text(
                                "BUY",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        )
                      ])))
            ],
          ),
        ));
  }
}
