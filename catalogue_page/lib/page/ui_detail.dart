import 'package:flutter/material.dart';

class uiDetail extends StatefulWidget {
  @override
  _uiDetailState createState() => _uiDetailState();
}

class _uiDetailState extends State<uiDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.deepPurple[400],
        title: new Text("Product"),
      ),
      body: new Container(
        height: double.infinity,
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          children: [
            Expanded(
                flex: 4,
                child: Container(
                  child: FittedBox(
                    child: FlutterLogo(),
                    fit: BoxFit.fill,
                  ),
                )),
            Expanded(
                flex: 6,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 70,
                        decoration: BoxDecoration(
                          color: Colors.deepPurple[400],
                        ),
                        child: Text(
                          "Product Title",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 24,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 50,
                        child: Text(
                          "IDR1.000.000",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 50,
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 10))),
                        child: Text(
                          "Stock : 10",
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 1))),
                        child: Text(
                          "Product Detail",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 10))),
                        child: Text(
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit...",
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 1))),
                        child: Text(
                          "Store Name",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 1))),
                        child: Text(
                          "Location",
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
