import 'package:catalogue_page/page/product_detail.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class ProductList extends StatefulWidget {
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  Future<List> getData() async {
    var data =
        await http.get(Uri.parse("http://127.0.0.1/catalog/ambildata.php"));
    var jsonData = json.decode(data.body);
    return jsonData;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () =>
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) => new detailPage(
                                  list: snapshot.data,
                                  index: index,
                                ))),
                    child: Container(
                      padding: EdgeInsets.all(10),
                      height: 160,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey[200], width: 2))),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 3,
                            child: Container(
                              width: 120,
                              height: 120,
                              decoration:
                                  BoxDecoration(color: Colors.deepPurple[400]),
                              child: Image(
                                image: (snapshot.data[index]["gambar"]),
                              ),
                            ),
                          ),
                          Expanded(
                              flex: 7,
                              child: Container(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                                  height: 120,
                                  child: Stack(children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        new Container(
                                            padding: EdgeInsets.fromLTRB(
                                                0, 0, 0, 20),
                                            child: Text(
                                              snapshot.data[index]
                                                  ["nama_barang"],
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            )),
                                        new Container(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 0, 0, 10),
                                          child: Text(NumberFormat.currency(
                                                  locale: 'id',
                                                  decimalDigits: 0)
                                              .format(snapshot.data[index]
                                                  ["harga"])),
                                        ),
                                        new Row(
                                          children: [
                                            Icon(
                                              Icons.location_on,
                                              color: Colors.grey[600],
                                              size: 20,
                                            ),
                                            Text(
                                              snapshot.data[index]["lokasi"],
                                              style: TextStyle(
                                                  color: Colors.grey[600]),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                    Positioned(
                                      bottom: 0,
                                      right: 0,
                                      child: Container(
                                        width: 50,
                                        height: 30,
                                        decoration: BoxDecoration(
                                            color: Colors.deepPurple[400]),
                                        child: Center(
                                          child: Text(
                                            "BUY",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ])))
                        ],
                      ),
                    ),
                  );
                });
          }
        },
      ),
    );
  }
}
