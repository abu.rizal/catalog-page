import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class detailPage extends StatefulWidget {
  List list;
  int index;
  detailPage({this.index, this.list});

  @override
  _detailPageState createState() => _detailPageState();
}

class _detailPageState extends State<detailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(widget.list[widget.index]['nama_barang']),
      ),
      body: new Container(
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          children: [
            Expanded(
                flex: 4,
                child: Container(
                  child: FittedBox(
                    child: FlutterLogo(),
                    fit: BoxFit.fill,
                  ),
                )),
            Expanded(
                flex: 6,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 70,
                        decoration: BoxDecoration(
                          color: Colors.deepPurple[400],
                        ),
                        child: Text(
                          widget.list[widget.index]['nama_barang'],
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 24,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 50,
                        child: Text(NumberFormat.currency(
                                locale: 'id', decimalDigits: 0)
                            .format(widget.list[widget.index]['lokasi'])),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        height: 50,
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 10))),
                        child: Text(
                          "Stock : " + widget.list[widget.index]['stok'],
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 1))),
                        child: Text(
                          "Product Detail",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 10))),
                        child: Text(
                          widget.list[widget.index]['desc'],
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 1))),
                        child: Text(
                          widget.list[widget.index]['nama_toko'],
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey[200], width: 1))),
                        child: Text(
                          widget.list[widget.index]['lokasi'],
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}

// widget.list[widget.index]['lokasi']